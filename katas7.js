// Sua tarefa será reproduzir o comportamento desses métodos de Array com suas funções callback - sem usar as versões incorporadas do JavaScript:

// fazer dese modo o katas 07
const numbers = [1, 2, 3, 4, 5];
let newArray;  
let newArray2;
let newArray3;
let newArray4;
function newforEach(array, callbackToRunOnEachItem) {
    for (let index = 0; index < array.length; index++) {
        const currentValue = array[index]
        
        // console.log(currentValue)
        // console.log(array)
        // console.log(index)
        callbackToRunOnEachItem(currentValue, index, numbers)
        // console.log(callbackToRunOnEachItem)
    }
}
newforEach(numbers, function (numbers, index, array) {
    const numberiseven = numbers % 2 == 0
    const numberType = numberiseven ? 'even' : 'odd'
    // console.log(numbers)
    console.log(`The number '${numbers}' at position ${index} of ${array} is a ${numberType}.`)
})
function newMap(array, callbacksquareroot) {
    newArray = []
    for (let index = 0; index < array.length; index++) {
        const currentValue = newArray.push(array[index])
        console.log(currentValue)
        console.log(newArray)
        callbacksquareroot(newArray, index, numbers)
    }
}
newMap(numbers, function (newArray, index, array) {
    // const squareroot = Math.sqrt(newArray[index])
    // console.log(squareroot)
    console.log(newArray, "novoarray")
     console.log(`The number '${newArray}' at position ${index} of ${array}`) //and sqrt is a ${squareroot}`)
})

console.log(newArray,"fora da function")

function newSome(array, callbackboolean) {
    for (let index = 0; index < array.length; index++) {
        const currentValue = array[index]
        callbackboolean(currentValue, index, array)

    }
}
newSome(numbers, function (numbers, index, array) {
    const biggerThen = numbers > 3
    const biggerThenThree = biggerThen
    console.log(`The number '${numbers}' at position ${index} of ${array} is bigger then three is a ${biggerThenThree}.`)
})
numbers2 = [5, 12, 8, 130, 44];
function newFind(array, callbackfind) {
    newArray2 = []
    
    for (let index = 0; index < array.length; index++) {
        // condição se o valor for maior que 4
        if( numbers[index] > 4) {
            // dar o push para adicionar no segundo array
        const currentValue = newArray2.push(array[index])
        console.log(currentValue)
        
        }
        console.log(newArray2, 'novoarray2')
        callbackfind(newArray2, index, array)
    }
}
newFind(numbers, function (newArray2, index, array) {
    const found = newArray2[0]
    // const foundmenosum = found ? newArray2[0]  : undefined
    console.log(newArray2, "novoarray2")
     console.log(`The number '${newArray2}' at position ${index} of ${array} is a ${found}`)
})
function newFindIndex(array, callbackfindindex) {
    newArray3 = []
    
    for (let index = 0; index < array.length; index++) {
        // condição se o index maior que 3
        if( numbers[index] > 4) {
            // dar o push para adicionar no segundo array
        const currentValue = newArray3.push(array[index])
        console.log(currentValue)
        }
        console.log(newArray3, 'novoarray3')
        callbackfindindex(newArray3, index, array)
    }
}
newFindIndex(numbers, function (newArray3, index, array) {
    const found1 = newArray3[0]
    const foundmenosum1 = found1 ? newArray3[0]  : -1
    console.log(newArray3, "novoarray3")
     console.log(`The number '${newArray3}' at position ${index} of ${array} is a ${foundmenosum1}`)
})
function newEvery(array, callbackevery) {
    for (let index = 0; index < array.length; index++) {
        const currentValue = array[index]
        callbackevery(currentValue, index, array)

    }
}
newEvery(numbers, function (numbers, index, array) {
    const smallestThen = numbers < 3
    const smallestThenThree = smallestThen ? true : false 
    console.log(`The number '${numbers}' at position ${index} of ${array} is smallest then three is a ${smallestThenThree}.`)
})

function newFilter(array, callbackfind) {
    newArray4 = []
    
    for (let index = 0; index < array.length; index++) {
        // condição se o index maior que 2
        if( index > 2) {
            // dar o push para adicionar no segundo array
        const currentValue = newArray4.push(array[index])
        console.log(currentValue)
        
        }
        console.log(newArray4, 'novoarray4')
        callbackfind(newArray4, index, array)
    }
}
newFilter(numbers, function (newArray4, index, array) {
    const found2 = newArray4[0]
    // const foundmenosum = found ? newArray4[0]  : undefined
    console.log(newArray4, "novoarray4")
     console.log(`The number '${newArray4}' at position ${index} of ${array} is a ${newArray4}`)
})
